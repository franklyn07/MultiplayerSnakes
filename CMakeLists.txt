cmake_minimum_required(VERSION 3.5.1)
project (MultiplayerSnakes C)

set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -pthread")

set(SERVER 
		./Server/Server.c
		./Server/LinkedList/LinkedList.c
		./Server/LinkedList/ActiveConnectionsList.c
		./Server/LinkedList/PositionSocketList.c
		./Server/LinkedList/PointsList.c
		./Server/LinkedList/DirectionsList.c
		./Server/Utility/Utility.c
		./Server/Utility/Serialization/Serialization.c
		./Server/Map/Map.c
		./Server/Threads/Threads_Server.c)


set(CLIENT
		./Client/Client.c
		./Client/Utility/Deserialization/Deserialization.c
		./Client/Utility/N_Curses/painter.c
		./Client/Threads/Threads_Client.c)

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

add_executable(snake_server ${SERVER})
add_executable(snake_client ${CLIENT})
target_link_libraries(snake_client ${CURSES_LIBRARIES})