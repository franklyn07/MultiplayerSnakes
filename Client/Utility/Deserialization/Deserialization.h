#ifndef DESERIALIZATION_CLIENT_H
#define DESERIALIZATION_CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/*Special Characters
/m - Map size
/c - current state of map
/w - Winner
/l - Loser
/n - new snake
/h - list of head positions
/t - list of tail positions
/f - fruit position
/r - remove snake*/

//deserializes map size
int deserialize_map_size(char * buffer);

//deserialize current map state
void deserialize_current_map_state(char * buffer, int** map);

//deserializes fruit position
void deserialize_fruit(char * buffer, int ** map);

//deserialize head and tail position
void deserialize_head(char* buffer, int ** map);
void deserialize_tail(char* buffer, int ** map);

//deserializes new snake
void deserialize_new_snake(char * buffer , int** map);

//remove snake from map
void deserialize_snake_to_remove(char* buffer, int ** map);

#endif