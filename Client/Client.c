#include "./Client.h"

int flag = 0;

int main(int argc, char *argv[])
{
    // Declare variables
    int sockfd, portno, returnVal;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    // Make sure that server name and port are available in command line arguments
    if (argc < 3) {
        fprintf(stderr,"usage %s hostname port\n", argv[0]);
        exit(0);
    }

    // Get port number 
    portno = atoi(argv[2]);

    // Create a socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }

    // Get server name 
    if ((server = gethostbyname(argv[1])) == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
		close(sockfd);
        exit(0);
    }

    // Populate serv_addr structure 
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;  // Set to AF_INET
    bcopy((char *) server -> h_addr, // Set server address
          (char *) &serv_addr.sin_addr.s_addr,
                   server -> h_length);
    serv_addr.sin_port = htons(portno); // Set port (convert to network byte ordering)

    // Connect to the server 
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
    {
         perror("ERROR connecting");
		 close(sockfd);
         exit(1);
    }

    //catches signal and handles it
    signal(SIGINT, signalHandler);

    /*pthread_t userInputThread;*/
    pthread_t getUpdatesThread;

    //create a thread that gets updates from the server
    if((returnVal = pthread_create(&getUpdatesThread, NULL, getUpdates, (void*)&sockfd))!=0){
        error("Get updates thread could not be created!");
        cancelConnection(sockfd);
        close(sockfd);
        exit(-1);
    }	

/*    //create a thread that gets the user input
    if((returnVal = pthread_create(&userInputThread, NULL, getUserInput, (void*)&sockfd))!=0){
        error("Listener thread could not be created!");
        cancelConnection(sockfd);
        close(sockfd);
        exit(-1);
    }*/

    //check if a signal is caught, if yes kill listener thread
    while(1){
        if(flag == 1){
            cancelConnection(sockfd);
            /*pthread_cancel(userInputThread);*/
            pthread_cancel(getUpdatesThread);
            break;
        }
    }

    //wait for threads to complete
   /* pthread_join(userInputThread,NULL);*/
    pthread_join(getUpdatesThread,NULL);

    return 0;
}


void error(char * message){
    perror(message);
}

//handles the signal 
void signalHandler(){
    //cancelling thread by setting flag
    flag = 1;

    printf("Caught signal and thread cancelled!\n");
    endwin();
}

void cancelConnection(int socket){
    char buffer [256];
    int n;

    buffer[0] = '/';
    buffer[1] = 'i';
    buffer[2] = 'u';
    buffer[3] = '\0';

    if ((n = write(socket,buffer,4)) < 0){
         perror("ERROR writing to socket");
    }
}