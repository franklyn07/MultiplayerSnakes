#ifndef THREADS_CLIENT_H
#define THREADS_CLIENT_H

#include "../Utility/Deserialization/Deserialization.h"
#include "../Client.h"
#include <string.h>
#include "../Utility/N_Curses/painter.h"

typedef struct map_update_data{
	char buffer [256];
	int** map ;
	int* points;
	int sockfd;
	int MAPSIZE;
}map_update_data_t;

//gets update from the server
void * getUpdates(void * arg);

//prints the map on the user screen
void * printMap(void * arg);

//updates current state of map
void * updateMap(void * arg);

//updates the fruit position on the map
void * updateFruit(void * arg);

//updates a new snake on the map
void * updateNewSnake(void * arg);

//updates new heads on the map
void * updateNewHeads(void * arg);

//updates new tails on the map
void * updateNewTails(void * arg);

//gets input from user
void * getUserInput(void * arg);

//removes snake
void * removeSnake(void * arg);

#endif