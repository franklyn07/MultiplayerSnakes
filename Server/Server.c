#include "./Server.h"

//will flag to 1 whenever a signal is caught
int flag = 0;

//enables mutex to lock variable for a specific thread
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char *argv[]){
	// Declare variables
	int sockfd, portno, returnVal;
	struct sockaddr_in serv_addr, cli_addr;

	// Get port from command line
	if (argc < 2) {
		error("Port not passed: ./Server <portno>\n");
		exit(-1);
	}

	// Create server socket
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		error("Unable to open socket!");
		exit(-1);
	}

	// Initialize socket structure
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	// Bind the host address
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("Could not bind socket!");
		close(sockfd);
		exit(-1);
	}

	//we initialise the map
	int ** map = map_init();

	//allocating memory for the struct and setting the values
	listen_data_t *tdata;
	tdata = (listen_data_t *) malloc (sizeof(listen_data_t));
	tdata -> local_sockfd = sockfd;
	tdata -> cli_addr = &cli_addr;
	tdata-> map = map;

	//creating thread to listen to clients
	pthread_t listener;

	if((returnVal = pthread_create(&listener, NULL, listenForClientsThread, (void*)tdata))!=0){
		error("Listener thread could not be created!");
		close(sockfd);
		exit(-1);
	}

	//catches signal
	signal(SIGINT,signalHandler);

	//check if a signal is caught, if yes kill listener thread
	while(1){
		if(flag == 1){
			pthread_cancel(listener);
			break;
		}
	}

	//wait for listener thread to complete
	pthread_join(listener,NULL);

	//map cleanup
	map_free(map);
	printf("Map has been cleaned\n");

	return 0; // we never get here
}

//handles the signal 
void signalHandler(){
	//cancelling thread by setting flag
	flag = 1;

	printf("Caught signal and thread cancelled!\n");
}

//function that displays the error
void error(char *msg){
	perror(msg);
}
