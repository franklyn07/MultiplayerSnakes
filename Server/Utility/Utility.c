#include "./Utility.h"

//takes head of linked list and transforms its first value contents into
//an array of integers
int * linkedListToArrayInt(node* head){
	node * current = head;

	//size of linked list counter
	int size =0;

    while (current != NULL) {
    	size+=1;
        current = current->next;
    }

    //create array of the size of the linked list
    int * arrayOfInt = malloc((size+1)*sizeof(*arrayOfInt));

    //checking malloc success
    if(arrayOfInt == NULL){
    	return NULL;
    }

    //reset pointer to head
    current = head;

    //assigning size of array in first block
    arrayOfInt[0]=size+1;

    //getting values from linked list and passing them to array
    for(int i = 1; i<size+1;i++){
    	arrayOfInt[i] = *(int *)current -> firstVal;
    	current = current -> next;
    }

    return arrayOfInt;
}

int generateRandomInt(int limit){
    time_t t;
    srand((unsigned) time(&t));
    return rand() % limit-1;
}

coordinate generateHead(int** map){
    
    for (int i = 0; i < 100000; i++){
        int x_coord = generateRandomInt(MAPSIZE);
        int y_coord = generateRandomInt(MAPSIZE);

        if (map[y_coord][x_coord] == 0 && x_coord > 2 && x_coord <= MAPSIZE-3 && y_coord > 2 && y_coord <= MAPSIZE-3){
            struct coordinate coord = {x_coord,y_coord};
            return coord;
        }
    }

    //if not found return -1,-1
    struct coordinate error_coord = {-1,-1};
    return error_coord;
}

coordinate generateFruit(int ** map){
    for (int i = 0; i < 100000; i++){
        int x_coord = generateRandomInt(MAPSIZE);
        int y_coord = generateRandomInt(MAPSIZE);

        //check coordinate is empty
        if (map[y_coord][x_coord] == 0){
            struct coordinate coord = {x_coord,y_coord};
            return coord;
        }
    }

    //if not found return -1,-1
    struct coordinate error_coord = {-1,-1};
    return error_coord;
}

//returns current direction of snake
char getDirection(int head_x, int head_y, int tail_x, int tail_y){
    if((head_x - tail_x) > 0){
        //snake is going to the right
        return 'd';
    }else if ((head_x - tail_x) < 0){
        //snake is going to the left
        return 'a';
    }else{
        //it is moving either up or down (same x axis)
        if((head_y - tail_y)>0){
            //snake is moving down
            return 's';
        }else{
            //snake is moving up
            return 'w';
        }

    }
}

//returns coordinates of actual body part next to head of snake
struct coordinate getBody(coordinate head, coordinate tail){
    int body_x =-1, body_y=-1;
    int head_x = head.x;
    int head_y = head.y;
    int tail_x = tail.x;
    int tail_y = tail.y;

    if((head_x - tail_x) == 0){
        //snake is vertical
        body_x = head_x;
        if((head_y - tail_y)>0){
            body_y = head_y -1;
        }else{
            body_y = head_y +1;
        }
    }else{
        //snake is horizontal
        body_y = head_y;
        if((head_x - tail_x)>0){
            body_x = head_x -1;
        }else{
            body_x = head_x +1;
        }
    }

    struct coordinate coord = {body_x,body_y};
    return coord;
}

//returns coordiantes of actual tail of snake
struct coordinate getTail(coordinate head, coordinate body){
    int tail_x = -1, tail_y =-1;
    int head_x = head.x;
    int head_y = head.y;
    int body_x = body.x;
    int body_y = body.y;

    if((head_x - body_x) == 0){
        //snake is vertical
        tail_x = body_x;
        if((head_y - tail_y)>0){
            tail_y = body_y -1;
        }else{
            tail_y = body_y +1;
        }
    }else{
        //snake is horizontal
        tail_y = body_y;
        if((head_x - tail_x)>0){
            tail_x = body_x -1;
        }else{
            tail_x = body_x +1;
        }
    }

    struct coordinate coord = {tail_x,tail_y};
    return coord;
}


//returns new tail by finding the body piece next to it
struct coordinate getNewTail(int** map, coordinate currentTail, int snakeId){
    if (snakeId == map[currentTail.y+1][currentTail.x]){
        struct coordinate new_coord = {currentTail.x,currentTail.y+1};
        return new_coord;
    }else if (snakeId == map[currentTail.y-1][currentTail.x]){
        struct coordinate new_coord = {currentTail.x,currentTail.y-1}; 
        return new_coord;
    }else if (snakeId == map[currentTail.y][currentTail.x+1]){
        struct coordinate new_coord = {currentTail.x+1,currentTail.y}; 
        return new_coord;
    }else if (snakeId == map[currentTail.y][currentTail.x-1]){
        struct coordinate new_coord = {currentTail.x-1,currentTail.y}; 
        return new_coord;
    }else{
        struct coordinate new_coord = {-1,-1};
        return new_coord;
    }
}

//remove snake from map, and add the coordinates to buffer
int removeSnake(int**map, int sockfd, char * buffer){
    buffer[0] = '/';
    buffer[1] = 'r';

    int counter = 2;

    for (int i = 0; i < MAPSIZE; ++i){
        for (int j = 0; j < MAPSIZE; ++j){
            if(map[i][j] == sockfd){
                //remove from local map
                map[i][j] = 0;

                if(j>9){
                    sprintf(buffer+counter,"%d",j);
                }else{
                    sprintf(buffer+counter,"%d",0);
                    sprintf(buffer+counter+1,"%d",j);
                }
                counter+=2;

                if(i>9){
                    sprintf(buffer+counter,"%d",i);
                }else{
                    sprintf(buffer+counter,"%d",0);
                    sprintf(buffer+counter+1,"%d",i);
                }
                counter+=2;

            }
        }
    }

    buffer[counter] = '\0';
    counter+=1;
    return counter;
}