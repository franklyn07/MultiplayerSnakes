#ifndef UTILITY_H
#define UTILITY_H

#include "../LinkedList/LinkedList.h"
#include <stdlib.h>
#include "../Map/Map.h"
#include <time.h>

//takes head of linked list and transforms its first value contents into
//an array of integers
int * linkedListToArrayInt(node* head);

//tries to find a random spot on the map that is empty
//for head of snake
struct coordinate generateHead(int** map);

//generates a fruit coordinate
coordinate generateFruit(int ** map);

//returns a random integer
int generateRandomInt(int limit);

//returns current direction of snake
char getDirection(int head_x, int head_y, int tail_x, int tail_y);

//returns coordinates of actual body part next to head of snake
struct coordinate getBody(coordinate head, coordinate tail);

//returns coordiantes of actual tail of snake
struct coordinate getTail(coordinate head, coordinate body);

//returns new tail by finding the body piece next to it
struct coordinate getNewTail(int** map, coordinate currentTail, int snakeId);

//remove snake from map, and add the coordinates to buffer
int removeSnake(int**map, int sockfd, char * buffer);

#endif