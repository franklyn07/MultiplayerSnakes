#include "PointsList.h"

//print a list of values
void print_list_points(Points * head) {
    Points * current = head;

    while (current != NULL) {
        printf("Socket: %d\t Points: %d\n",current->socket,current->points);
        fflush(stdout);
        current = current->next;
    }
}

//adds node to current linked list
void addPlayerPoints(Points* head, Points* newPlayer){
    Points* current = head;


    //iterate through linked list to go to final node
    while (current->next != NULL) {
        current = current->next;
    }

    //add new node to end of list
    current->next = newPlayer;
}

//remove a specific player from list using socket 
//0 on success and -1 if not successful
int removePoints(Points ** head, int socket){
    for(Points ** current = head; *current != NULL; current = &(*current)->next){
        if((*current)->socket == socket){
            Points * next = (*current)->next;
            free (*current);
            *current = next;
            return 0;
        }
    }
    return -1;
}

//update position, after finding the socket
//return 0 on succes and -1 if not successful
int updatePoints(Points* head, int socket, int newPoints){
    Points * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            current->points = newPoints;
            return 0;
        }
        current = current->next;
    }

    return -1;
}

int getCurrentPoints(Points* head, int socket){
    Points * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            return current->points;
        }
        current = current->next;
    }

    return -1;
}