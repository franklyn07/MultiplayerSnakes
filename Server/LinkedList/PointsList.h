#ifndef POINTS_LIST_H
#define POINTS_LIST_H

#include <stdlib.h>
#include <stdio.h>

//will hold the socket, and points of player - can be head or tail
typedef struct Points{
	int socket;
	int points;
	struct Points * next;
}Points;

//will iterate over the list and print its values
//debugging purposes
void print_list_points(Points * head);

//remove a specific player from list using socket 
//0 on success and -1 if not successful
int removePoints(Points ** head, int socket);

//update points, after finding the socket
//return 0 on succes and -1 if not successful
int updatePoints(Points* head, int socket, int newPoints);

//adds node to current linked list
void addPlayerPoints(Points* head, Points* newPlayer);

//gets current amount of points
int getCurrentPoints(Points* head, int socket);

#endif