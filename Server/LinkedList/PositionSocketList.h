#ifndef COORD_LIST_H
#define COORD_LIST_H

#include <stdlib.h>
#include <stdio.h>
#include "../Map/Map.h"

//will hold the socket, and coordinate of snake - can be head or tail
typedef struct SnakePosition{
	int socket;
	int x_coord;
	int y_coord;
	struct SnakePosition * next;
}SnakePosition;

//will iterate over the list and print its values
//debugging purposes
void print_list_positions(SnakePosition * head);

//remove a specific position from list using socket 
//0 on success and -1 if not successful
int removePosition(SnakePosition ** head, int socket);

//update position, after finding the socket
//return 0 on succes and -1 if not successful
int updatePosition(SnakePosition* head, int socket, coordinate coord);

//adds node to current linked list
void addPosition(SnakePosition* head, SnakePosition* position);

//returns current position
coordinate getCurrentPosition(SnakePosition* head, int socket);

#endif