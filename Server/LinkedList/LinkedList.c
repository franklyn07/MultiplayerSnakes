#include "LinkedList.h"

//print a list of values
void print_list(node * head) {
    node * current = head;

    while (current != NULL) {
        printf("%d\n", *(int *)current->firstVal);
        current = current->next;
    }
}

void push1ValuedItem(node * head, void* val) {
    //start at head
    node * current = head;
    
    //iterate to last value
    while (current->next != NULL) {
        current = current->next;
    }

    // now we can add a new variable 
    current->next = malloc(sizeof(node));
    current->next->firstVal = (void *)val;
    current->next->next = NULL;
}

void push2ValuedItem(node * head, void * val1, void * val2) {
    //start at head
    node * current = head;
    
    //iterate to last value
    while (current->next != NULL) {
        current = current->next;
    }

    /* now we can add a new variable */
    current->next = malloc(sizeof(node));
    current->next->firstVal = val1;
    current->next->secondVal = val2;
    current->next->next = NULL;
}


int remove_by_value(node ** head, void* val) {
    node *previous, *current;

    if (*head == NULL) {
        return -1;
    }

    if ((*head)->firstVal == val) {
        (*head)->firstVal = NULL;
        return 0;
    }

    previous = current = (*head)->next;
    while (current) {
        if (current->firstVal == val) {
            previous->next = current->next;
            free(current);
            return 0;
        }

        previous = current;
        current  = current->next;
    }
    return -1;
}

int updateSecondVal(node* head, void* toFind, void * toAdd){
	node * current = head;

	//iterate through linked list
    while (current != NULL) {
    	//if we find the value update the second value
       	if(current->firstVal == toFind){
       		current->secondVal = toAdd;
       		return 0;
       	}
       	current = current->next;
    }

    return -1;
}

//we search for an item and we get the second value in the same node
void * getMapping(node* head, void * val){
	node * current = head;

	//iterate through linked list
    while (current != NULL) {
    	//if we find the value update the second value
       	if(current->firstVal == val){
       		return current->secondVal;
       	}
       	current = current->next;
    }

    return NULL;
}