#include "ActiveConnectionsList.h"

//print a list of values
void print_list_sockets(struct ActiveConnection * head) {
    struct ActiveConnection * current = head;

    while (current != NULL) {
        printf("Socket, thread, status: %d,%lu,%d\n",current->socket, current->thread, current -> toDelete);
        current = current->next;
    }
}

void pushThread(struct ActiveConnection * head, pthread_t thread) {
    //start at head
    struct ActiveConnection * current = head;
    
    //iterate to last value
    while (current != NULL) {
        current = current->next;
    }

    // now we can add a new variable 
    current = malloc(sizeof(ActiveConnection));
    current->thread = thread;
    current->socket = -1;
    current->next = NULL;
}

void pushThread_Socket(struct ActiveConnection * head, pthread_t thread, int socket) {
    //start at head
    struct ActiveConnection * current = head;
    
    //iterate to last value
    while (current->next != NULL) {
        current = current->next;
    }

    /* now we can add a new variable */
    struct ActiveConnection * newConnection = malloc(sizeof(ActiveConnection));
    newConnection->thread = thread;
    newConnection->socket = socket;
    newConnection->toDelete = 0;
    newConnection->next = NULL;
    current->next = newConnection;
}


int removeSocket(struct ActiveConnection ** head, int socket) {

    for(ActiveConnection ** current = head; *current != NULL; current = &(*current)->next){
        if((*current)->socket == socket){
            ActiveConnection * next = (*current)->next;
            free (*current);
            *current = next;
            return 0;
        }
    }
    return -1;
}

int updateSocket(struct ActiveConnection* head, pthread_t thread, int socket){
	struct ActiveConnection * current = head;

	//iterate through linked list
    while (current != NULL) {
    	//if we find the value update the second value
       	if(current->thread == thread){
       		current->socket = socket;
       		return 0;
       	}
       	current = current->next;
    }

    return -1;
}

//we search for a socket and get the thread in the same active connection
pthread_t getThread(struct ActiveConnection* head, int socket){
    struct ActiveConnection * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            return current->thread;
        }
        current = current->next;
    }

    return (long unsigned)-1;
}

//we search for an item and we get the second value in the same ActiveConnection
int getSocket(struct ActiveConnection* head, pthread_t thread){
	struct ActiveConnection * current = head;

	//iterate through linked list
    while (current != NULL) {
    	//if we find the value update the second value
       	if(current->thread == thread){
       		return current->socket;
       	}
       	current = current->next;
    }

    return -1;
}

//adds node to current linked list
void addConnection(struct ActiveConnection* head,struct ActiveConnection* connection){
    struct ActiveConnection * current = head;

        //iterate through linked list to go to final node
        while (current != NULL) {
            current = current->next;
        }

        //add new node to end of list
        current->next = connection;
}

//gets number of active connections
int getNumActiveConnections(struct ActiveConnection* head){
    struct ActiveConnection * current = head;
    int size = 0;

        //iterate through linked list to go to final node
        while (current != NULL) {
            if(current->socket > 0){
                size+=1;
            }
            current = current->next;
        }
    return size;
}

//sets toDelete value to 1
void setToDelete(struct ActiveConnection* head, int socket){
    struct ActiveConnection * current = head;

    //iterate through linked list to go to final node
    while (current != NULL) {
        if(current->socket == socket){
           current->toDelete = 1;
           printf("Set to delete\n");
           break;
        }
        current = current -> next;
    }
}