#include "DirectionsList.h"

//print a list of values
void print_list_directions(Direction * head) {
    Direction * current = head;

    while (current != NULL) {
        printf("Socket: %d\t Direction: %c\n",current->socket,current->direction);
        fflush(stdout);
        current = current->next;
    }
}

//adds node to current linked list
void addPlayerDirection(Direction* head, Direction* newPlayer){
    Direction* current = head;


    //iterate through linked list to go to final node
    while (current->next != NULL) {
        current = current->next;
    }

    //add new node to end of list
    current->next = newPlayer;
}

//remove a specific player from list using socket 
//0 on success and -1 if not successful
int removePlayer(Direction ** head, int socket){
    for(Direction ** current = head; *current != NULL; current = &(*current)->next){
        if((*current)->socket == socket){
            Direction * next = (*current)->next;
            free (*current);
            *current = next;
            return 0;
        }
    }
    return -1;
}

//update direction, after finding the socket
//return 0 on succes and -1 if not successful
int updateDirection(Direction* head, int socket, char newDirection){
    Direction * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            current->direction = newDirection;
            return 0;
        }
        current = current->next;
    }

    return -1;
}

//returns last direction of player - if not succesful return null char
char getLastDirection(Direction* head, int socket){
    Direction * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value return second value
        if(current->socket == socket){
            return (char)current->direction;
        }
        current = current->next;
    }

    return '\0';
}