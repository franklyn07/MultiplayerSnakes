#ifndef GEN_LINKEDLIST_H
#define GEN_LINKEDLIST_H

#include <stdlib.h>
#include <stdio.h>

//nodes that will hold generic data in the linked list
typedef struct node{
	void * firstVal;
	void * secondVal;
	struct node * next;
}node;

//will iterate over the list and print its values
//debugging purposes
void print_list(node * head);

//will add item with 1 value to the end of the list
void push1ValuedItem(node * head, void * val);

//will add item with 2 value to the end of the list
void push2ValuedItem(node * head, void * val1, void * val2);

//remove a specific item from the list using value
//return 0 on succes and -1 if not successful
int removeUsingVal(node ** head, void * val);

//update second value of node, after finding first val
//return 0 on succes and -1 if not successful
int updateSecondVal(node* head, void* toFind, void * toAdd);

//we search for an item and we get the second value in the same node
void * getMapping(node* head, void * val);

#endif