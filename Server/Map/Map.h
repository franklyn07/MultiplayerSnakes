#ifndef MAP_H
#define MAP_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

#define MAPSIZE 20

//will hold the x and y coordinate pairs
typedef struct coordinate{
    int x;
    int y;
}coordinate;


//initialises map
int ** map_init();

//empties map
void map_free(int ** map);

//add snake to map and return the coordinate of its tail
coordinate addSnake(int id, coordinate head, int** map);

//print map for debugging purposes
void printMap(int** map);

#endif