#include "./Map.h"

//initialises map
int ** map_init(){
	int ** map = (int**)malloc(MAPSIZE*sizeof(int*));
	
	//allocating memory
	for(int i = 0; i< MAPSIZE; i++){
		map[i] = (int*)malloc(MAPSIZE*sizeof(int));
	}

	for(int i = 0; i< MAPSIZE; i++){
		for(int j = 0; j < MAPSIZE; j++){
			//0 means empty
			map[i][j] =  0;
		}
	}

	printf("MAPSIZE %d\n",MAPSIZE );
	fflush(stdout);
	return map;
}

//empties map
void map_free(int ** map){
	// first free each row
    for (int i = 0; i < MAPSIZE; i++) {
         free(map[i]);
    }

    // Eventually free the memory of the pointers to the rows
    free(map);
}

coordinate addSnake(int id, coordinate head, int** map){
	//get head coordinate
	int x_coord = head.x;
	int y_coord = head.y;

	time_t t;
	srand((unsigned) time (&t));

	for(int i = 0; i < MAPSIZE; i++){
		for(int j = 0; j<MAPSIZE; j++){
			if((j == x_coord)&&(i == y_coord)){
				map[i][j] = id;
			}
		}
	}

	//generate random direction for body to be placed
	int x_coord_1,x_coord_2,y_coord_1,y_coord_2;
	int random;

	//between 0 and 3
	random = rand()%3;

	switch(random){
		case 0:
			x_coord_1 = x_coord -1;
			x_coord_2 = x_coord -2;
			y_coord_1 = y_coord_2 =y_coord;
			break;
		case 1:
			x_coord_1 = x_coord +1;
			x_coord_2 = x_coord +2;
			y_coord_1 = y_coord_2 =y_coord;
			break;
		case 2:
			y_coord_1 = y_coord -1;
			y_coord_2 = y_coord -2;
			x_coord_1 = x_coord_2 =x_coord;
			break;
		case 3:
			y_coord_1 = y_coord +1;
			y_coord_2 = y_coord +2;
			x_coord_1 = x_coord_2 =x_coord;
			break;
	}

	//placing first body part
	for(int i = 0; i < MAPSIZE; i++){
		for(int j = 0; j<MAPSIZE; j++){
			if((j == x_coord_1)&&(i == y_coord_1)){
				map[i][j] = id;
			}
		}
	}

	//placing tail
	for(int i = 0; i < MAPSIZE; i++){
		for(int j = 0; j<MAPSIZE; j++){
			if((j == x_coord_2)&&(i == y_coord_2)){
				map[i][j] = id;
			}
		}
	}

	//returning tail position
	/*Note that this will be used in list of tails,
	which always needs to be lagging behind by 1 place.
	The reason for this is, that the client will paint
	this coordinate black, aka removing one piece from
	behind, rather than actually painting the last piece.
	Therefore we need to modify the tail piece by moving
	it back by 1 place.*/
	struct coordinate coord ;

	switch(random){
		case 0:
			coord.x =x_coord_2-1;
			coord.y =y_coord_2;
			break;
		case 1:
			coord.x =x_coord_2+1;
			coord.y =y_coord_2;
			break;
		case 2:
			coord.x =x_coord_2;
			coord.y =y_coord_2-1;
			break;
		case 3:
			coord.x =x_coord_2;
			coord.y =y_coord_2+1;
			break;
	}

	return coord;
}

void printMap(int ** map){
	for(int i = 0; i< MAPSIZE; i++){
		for(int j = 0 ; j<MAPSIZE;j++){
			printf("%d\t",map[i][j] );
		}
		printf("\n");
	}
}