#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include "./Utility/Utility.h"
#include "./Map/Map.h"
#include "./Threads/Threads_Server.h"

//signal handler
void signalHandler();

//function that displays the error
void error(char *msg);

#endif
