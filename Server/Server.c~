#include "./Server.h"

//will flag to 1 whenever a signal is caught
int flag = 0;

//enables mutex to lock variable for a specific thread
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char *argv[]){
	//buffer was not printing out
	//setbuf(stdout, NULL);

  // Declare variables
  int sockfd, portno, returnVal;
  struct sockaddr_in serv_addr, cli_addr;

  // Get port from command line
  if (argc < 2) {
    error("Port not passed: ./Server <portno>\n");
  }

  	// Create server socket
  	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    	error("Unable to open socket!");
	}

  // Initialize socket structure
  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = atoi(argv[1]);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

	// Bind the host address
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		error("Could not bind socket!");
	}

  	pthread_t listener;

  	//allocating memory for the struct and setting the values
	listen_data_t *tdata;
	tdata = (listen_data_t *) malloc (sizeof(listen_data_t));
	tdata -> local_sockfd = sockfd;
	tdata -> cli_addr = &cli_addr;

  	if((returnVal = pthread_create(&listener, NULL, listenForClientsThread, (void*)tdata))!=0){
  		error("Listener thread could not be created!");
  	}
  	
  	//catches signal
  	signal(SIGINT,signalHandler);

  	//check if a signal is caught, if yes kill listener thread
  	while(1){
  		if(flag == 1){
  			pthread_cancel(listener);
  			break;
  		}
  	}

  	//wait for listener thread to complete
  	pthread_join(listener,NULL);

  	return 0; // we never get here
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connection has been established.
 *****************************************/
void dostuff (int sock)
{
   int n;
   char buffer[256];
      
   bzero(buffer,256);
   
   while(1){
	   if ((n = read(sock,buffer,255)) < 0) {
		   close(sock);
		   error("ERROR reading from socket");
		   break;
	   }
	   
	   if(buffer[0]=='$'){	   
	   		printf("User Stopped Transmission");
	   		fflush(stdout);
	   		if ((n = write(sock,"$",15)) < 0) {
		   		close(sock);
	       		error("ERROR writing to socket");
	   		}
	   		break;
	   }

	   sleep(5);
	   
	   printf("Here is the message from sock %d: %s\n",sock,buffer);
	   
	   if ((n = write(sock,"I got your message",18)) < 0) {
		   close(sock);
	       error("ERROR writing to socket");
	       break;
	   }
	}
}

//function that displays the error
void error(char *msg){
    perror(msg);
}

//thread function that listens to connections
void *listenForClientsThread(void* arg){
  	//linked list of client socket ids
  	struct ActiveConnection * connectionsHead = NULL;

  	connectionsHead = malloc(sizeof(ActiveConnection));

  	//check if malloc is succesful
  	if (connectionsHead == NULL){
  		error("Cannot allocate memory to linked list!");
  		return;
  	}

  	connectionsHead -> thread = NULL;
  	connectionsHead -> socket = -1;
  	connectionsHead -> next = NULL;

  	//create struct which holds listener and list of connections
  	//to be able to cleanup if anything happens
  	struct clean_threads_data * cleanConnectionsData = malloc(sizeof(clean_threads_data));
  	
  	//check if malloc is succesful
  	if (cleanConnectionsData == NULL){
  		error("Cannot allocate memory cleaner!");
  		return;
  	}

  	cleanConnectionsData -> socketOfListener = *(int*)arg;
  	cleanConnectionsData -> connectionsHead = connectionsHead;

	//will push thread on stack just in case early exit
	pthread_cleanup_push(listen_thread_cleanup,(void*)cleanConnectionsData);

	//will hold return from creating thread - if 0 created correctly
	int returnVal;

	//will hold address of new socket of client
	int newsockfd;

	//will hold thread temporarily
	pthread_t client;

	//get listen data and convert it
	listen_data_t * data = (listen_data_t*) arg;
  	
	//assign values
  	int sockfd = data->local_sockfd;
  	socklen_t clilen = sizeof((data->cli_addr));

 	// Start listening for the clients
  	listen(sockfd,MAPSIZE);

  	//catch signal
  	signal(SIGINT,signalHandler);

  	//accept clients
  	while(1){
  		// Accept connection from a client
  		newsockfd = accept(sockfd, (struct sockaddr *) data->cli_addr, &clilen);
  		if (newsockfd < 0){
  			error("Could not accept client!");
  		}else{
  			//create data for thread
  			client_data_t * clientData = malloc(sizeof(client_data_t));
  	
		  	//check if malloc is succesful
		  	if (clientData == NULL){
		  		error("Cannot allocate memory cleaner!");
		  		return;
		  	}

  			clientData -> local_sockfd = newsockfd;
  			clientData -> connectionsHead = connectionsHead;

  			//create thread to handle the client
			returnVal = pthread_create(&client, NULL, handleClientThread, (void *) clientData);

  			//creating the thread
			if(returnVal!=0){
				//delete new socket
				close(newsockfd);
				error("Could not create thread to handle client!");
			}
  		}
  	}

  	pthread_cleanup_pop(1);
}

//thread that handles client
void *handleClientThread(void* arg){
	//pushing thread on stack so that it can be cleaned if exit form outside
	pthread_cleanup_push(client_thread_cleanup, arg);

	client_data_t * clientData = (client_data_t*)arg;

	int sockfd = clientData -> local_sockfd;

	//push thread data on shared linked list
	//using locks since shared
	pthread_mutex_lock(&lock);
	pushThread_Socket(clientData -> connectionsHead, pthread_self(),sockfd);
	pthread_mutex_unlock(&lock);

	dostuff(sockfd);
	close(sockfd);
	pthread_exit(NULL);
	printf("Exit Normally");

	//popping from stack if early kill
	pthread_cleanup_pop(1);
}

//client handling thread cleaner
void listen_thread_cleanup(void * arg){
	//extracting data
	struct clean_threads_data* data = (struct clean_threads_data *) arg;

	//close all threads
	ActiveConnection * current = data->connectionsHead;

	//to skip empty head
	current = current->next;

    while (current != NULL) {
    	//cancel thread which will invoke socket closing
    	pthread_cancel(current->thread);
        current = current->next;
    }

	//close listener socket
	close(data->socketOfListener);
	printf("Cleaning Up Socket: %d\n", data->socketOfListener);
	fflush(stdout);
}

//client handling thread cleaner
void client_thread_cleanup(void * arg){
	int* sockfd = (int *) arg;
	//close socket
	close(*sockfd);
	printf("Cleaning Up Socket: %d\n", *sockfd);
	fflush(stdout);
}

//handles the signal 
void signalHandler(){
	//cancelling thread by setting flag
	flag = 1;

	printf("Caught signal and thread cancelled!\n");
}

