#include "./Threads_Client.h"

//lock for synch
pthread_mutex_t client_lock = PTHREAD_MUTEX_INITIALIZER;

//gets update from the server
void * getUpdates(void * arg){
	//serves as a flag for iteration
	int iterate = 0;

	//get socket id
	int sockfd = *(int *) arg;

	//will hold data
	char buffer[256];

	//will hold retrunval from thread creation
	int returnVal;

	//will hold return from read
	int n;

    // Read mapsize from server
    bzero(buffer,256);
    if ((n = read(sockfd,buffer,256)) < 0)
    {
         perror("ERROR reading from socket");
         close(sockfd);
         pthread_exit(NULL);
    }

	//will hold mapsize - starts with a default 20
	int MAPSIZE = -1;

	//will hold buffer contents for mapsize
	char currentBuff [256];

	bzero(currentBuff,256);

	memcpy(currentBuff,buffer,256);

	pthread_mutex_lock(&client_lock);
	MAPSIZE = deserialize_map_size(currentBuff);
	pthread_mutex_unlock(&client_lock);

	if(MAPSIZE == -1){
		error("Server not responding! Please try again!\n");
		close(sockfd);
		pthread_exit(NULL);
	}

	//create local variable that holds map cache
	int ** map = (int**)malloc(MAPSIZE*sizeof(int*));
	
	//allocating memory
	for(int i = 0; i< MAPSIZE; i++){
		map[i] = (int*)malloc(MAPSIZE*sizeof(int));
	}

	for(int i = 0; i< MAPSIZE; i++){
		for(int j = 0; j < MAPSIZE; j++){
			//0 means empty
			map[i][j] = 0;
		}
	}

	//local variable that holds the points a client has
	int points = 0;

	//struct that will hold map update data
	map_update_data_t * new_update = (map_update_data_t *) malloc (sizeof(map_update_data_t));

	new_update -> map = map;
	new_update -> points = &points;
	new_update -> sockfd = sockfd;
	new_update -> MAPSIZE = MAPSIZE;

	//thread space to create any neccessary space
	pthread_t process, printer, userInput;

	//start printing map
	if((returnVal = pthread_create(&printer, NULL, printMap, (void*)new_update))!=0){
		error("Map update thread could not be created!");
	}

	//start polling user for input
	if((returnVal = pthread_create(&userInput, NULL, getUserInput, (void*)new_update))!=0){
		error("Input scanner thread could not be created!");
	}

	//loop for updates from server
	while(iterate == 0){
		// Read response from server response
        bzero(buffer,256);
        if ((n = read(sockfd,buffer,256)) < 0)
        {
             perror("ERROR reading from socket");
             close(sockfd);
             exit(1);
        }

        //struct that will hold map update data
		map_update_data_t * update = (map_update_data_t *) malloc (sizeof(map_update_data_t));

		memcpy(update -> buffer,buffer,256);
		update -> map = map;
		update -> points = &points;
		update -> sockfd = sockfd;
		update -> MAPSIZE = MAPSIZE;

        //if we have a flag check what type it is
        //else print the output out
        if(*buffer == '/'){
        	switch(*(buffer+1)){
        		case 'c':
        			if((returnVal = pthread_create(&process, NULL, updateMap, (void*)update))!=0){
						error("New snake update thread could not be created!");
					}
					break;
				case 'n':
					if((returnVal = pthread_create(&process, NULL, updateNewSnake, (void*)update))!=0){
						error("New snake update thread could not be created!");
					}
					break;
				case 'f':
					if((returnVal = pthread_create(&process, NULL, updateFruit, (void*)update))!=0){
						error("New fruit update thread could not be created!");
					}
					break;
				case 'h':
					if((returnVal = pthread_create(&process, NULL, updateNewHeads, (void*)update))!=0){
						error("New heads update thread could not be created!");
					}
					break;
				case 't':
					if((returnVal = pthread_create(&process, NULL, updateNewTails, (void*)update))!=0){
						error("New tails update thread could not be created!");
					}
					break;
				case 'r':
					if((returnVal = pthread_create(&process, NULL, removeSnake, (void*)update))!=0){
						error("New tails update thread could not be created!");
					}
					break;
				case 'l':
					printf("\nYOU LOST!CLOSING DOWN CLIENT!\n");
					iterate = 1;
					break;
				case 'p':
					//increase points by 1
					points+=1;
					break;
				case 'w':
					printf("\nYOU WON! CLOSING DOWN CLIENT!\n");
					iterate =1;
					break;
				case 'e':
					printf("\nSERVER DOWN! CLOSING DOWN CLIENT!\n");
					iterate =1;
					break;
				default:
					break;
        	}
        }

        //this sleep enables the threads the get hold of the lock on memory
        sleep(0.2);
	}

	//free map
	// first free each row
    for (int i = 0; i < MAPSIZE; i++) {
         free(map[i]);
    }

    // Eventually free the memory of the pointers to the rows
    free(map);

    //close the major ongoing threads
	pthread_cancel(printer);
	pthread_cancel(userInput);

	pthread_exit(NULL);
}

//prints the map on the user screen
void * printMap(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	mapPrinter(update->map, update -> MAPSIZE, update->sockfd, update -> points);

	pthread_exit(NULL);
}

//updates map
void * updateMap(void *arg){
	map_update_data_t * update = (map_update_data_t *)arg;

	pthread_mutex_lock(&client_lock);
	deserialize_current_map_state(update->buffer ,update->map);
	pthread_mutex_unlock(&client_lock);

	pthread_exit(NULL);
}

//updates the fruit position on the map
void * updateFruit(void * arg){
	map_update_data_t * update = (map_update_data_t *)arg;
	
	pthread_mutex_lock(&client_lock);
	deserialize_fruit(update->buffer,update->map);
	pthread_mutex_unlock(&client_lock);

	pthread_exit(NULL);
}

//updates a new snake on the map
void * updateNewSnake(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	pthread_mutex_lock(&client_lock);
	//deserialize map to get cache
	deserialize_new_snake(update->buffer ,update->map);
	pthread_mutex_unlock(&client_lock);

	pthread_exit(NULL);
}

//updates new heads on the map
void * updateNewHeads(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	pthread_mutex_lock(&client_lock);
	//deserialize map to get cache
	deserialize_head(update->buffer,update->map);
	pthread_mutex_unlock(&client_lock);
	pthread_exit(NULL);

}

//updates new tails on the map
void * updateNewTails(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	pthread_mutex_lock(&client_lock);
	//deserialize map to get cache
	deserialize_tail(update->buffer ,update->map);
	pthread_mutex_unlock(&client_lock);
	pthread_exit(NULL);

}

//removes snake
void * removeSnake(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	pthread_mutex_lock(&client_lock);
	//deserialize map to get cache
	deserialize_snake_to_remove(update->buffer ,update->map);
	pthread_mutex_unlock(&client_lock);
	pthread_exit(NULL);
}

//gets input from user
void * getUserInput(void * arg){
	//convert argument back to struct
	map_update_data_t * update = (map_update_data_t *) arg;

	//will hold buffer contents for mapsize
	char buffer [256];
	int n;
	char temp;

	//dont echo key presses
	noecho();
	//cbreak();

	//poll user for input
	while(1){
		bzero(buffer,256);

		buffer[0] = '/';
		buffer[1] = 'i';

		temp = getch();

		sleep(0.02);

		if (temp == (char)'w'||temp==(char)'a'||temp==(char)'s'||temp==(char)'d'){
			buffer[2] = temp;
			buffer[3] = '\0';
			if ((n = write(update->sockfd,buffer,4)) < 0){
             	perror("ERROR writing to socket");
        	}
		};

	}

	pthread_exit(NULL);

}
