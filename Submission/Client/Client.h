#ifndef CLIENT_C_H
#define CLIENT_C_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include "./Utility/Deserialization/Deserialization.h"
#include "./Threads/Threads_Client.h"
#include <ncurses.h>

//prints the error
void error(char* errorMessage);

//handles signal interrupt
void signalHandler();

//handles connection ending
void cancelConnection(int socket);


#endif