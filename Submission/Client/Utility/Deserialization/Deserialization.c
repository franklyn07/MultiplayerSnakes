#include "./Deserialization.h"

//deserializes map
int deserialize_map_size(char * buffer){
	//start at two since first two characters in buffer are special characters

	//this checking is added since the function is also used to check when
	//the client could not connect to server
	if(buffer[0] != '/' && buffer[1] != 'm'){
		return -1;
	}

	int tens;
	int ones;

	tens = buffer[2] - '0';

	if(buffer[3] == '\0'){
		return tens*10;
	}

	ones = buffer[3] - '0';

	return (tens*10)+ones;
}

//deserialize current map state
void deserialize_current_map_state(char * buffer, int** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int count = 2;

	int id_a,id_b,x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	int id, x_coord,y_coord;

	while(1){
		char current;
		current = buffer[count];

		if(current == '\0'){
			break;
		}

		id_a = buffer[count] - '0';
		id_b = buffer[count+1] - '0';
		if(id_a == 0){
			id = id_b;
		}else{
			id = (id_a*10)+id_b;

		}
		count+=2;

		x_coord_a = buffer[count] - '0';
		x_coord_b = buffer[count+1] - '0';
		if(x_coord_a == 0){
			x_coord = x_coord_b;
		}else{
			x_coord = (x_coord_a*10)+x_coord_b;
		}
		count+=2;

		y_coord_a = buffer[count] - '0';
		y_coord_b = buffer[count+1] - '0';
		if(y_coord_a == 0){
			y_coord = y_coord_b;
		}else{
			y_coord = (y_coord_a*10)+y_coord_b;
		}
		count+=2;

		map[y_coord][x_coord] = id;
	}
}

//deserializes fruit position
void deserialize_fruit(char * buffer, int ** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int count = 2;

	int x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	int x_coord,y_coord;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		x_coord = x_coord_b;
	}else{
		x_coord = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		y_coord = y_coord_b;
	}else{
		y_coord = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	//assign value to map
	map[y_coord][x_coord] = 126;
}

//deserialize snake to remove
void deserialize_snake_to_remove(char* buffer, int ** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int count = 2;

	int x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	int x_coord,y_coord;

	while(1){
		char current;
		current = buffer[count];

		if(current == '\0'){
			break;
		}

		x_coord_a = buffer[count] - '0';
		x_coord_b = buffer[count+1] - '0';
		if(x_coord_a == 0){
			x_coord = x_coord_b;
		}else{
			x_coord = (x_coord_a*10)+x_coord_b;
		}
		count+=2;

		y_coord_a = buffer[count] - '0';
		y_coord_b = buffer[count+1] - '0';
		if(y_coord_a == 0){
			y_coord = y_coord_b;
		}else{
			y_coord = (y_coord_a*10)+y_coord_b;
		}
		count+=2;

		map[y_coord][x_coord] = 0;
	}
}

void deserialize_head(char* buffer, int ** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int head_x,head_y, idNewSnake;

	int count = 2;

	int id_a,id_b,x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		head_x = x_coord_b;
	}else{
		head_x = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		head_y = y_coord_b;
	}else{
		head_y = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	id_a = buffer[count] - '0';
	id_b = buffer[count+1] - '0';
	if(id_a == 0){
		idNewSnake = id_b;
	}else{
		idNewSnake = (id_a*10)+id_b;
	}
	count+=2;

	map[head_y][head_x] = idNewSnake;
}

void deserialize_tail(char* buffer, int ** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int head_x,head_y;

	int count = 2;

	int x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		head_x = x_coord_b;
	}else{
		head_x = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		head_y = y_coord_b;
	}else{
		head_y = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	map[head_y][head_x] = 0;
}

//deserializes new snake
void deserialize_new_snake(char * buffer, int** map){
	//printf("Buffer to deseriailize%s\n",buffer );

	int head_x,head_y,body_x,body_y,tail_x,tail_y,idNewSnake;

	int count = 2;

	int id_a,id_b,x_coord_a,x_coord_b,y_coord_a,y_coord_b;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		head_x = x_coord_b;
	}else{
		head_x = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		head_y = y_coord_b;
	}else{
		head_y = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		body_x = x_coord_b;
	}else{
		body_x = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		body_y = y_coord_b;
	}else{
		body_y = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	x_coord_a = buffer[count] - '0';
	x_coord_b = buffer[count+1] - '0';
	if(x_coord_a == 0){
		tail_x = x_coord_b;
	}else{
		tail_x = (x_coord_a*10)+x_coord_b;
	}
	count+=2;

	y_coord_a = buffer[count] - '0';
	y_coord_b = buffer[count+1] - '0';
	if(y_coord_a == 0){
		tail_y = y_coord_b;
	}else{
		tail_y = (y_coord_a*10)+y_coord_b;
	}
	count+=2;

	id_a = buffer[count] - '0';
	id_b = buffer[count+1] - '0';
	if(id_a == 0){
		idNewSnake = id_b;
	}else{
		idNewSnake = (id_a*10)+id_b;
	}
	count+=2;

	map[head_y][head_x] = idNewSnake;
	map[body_y][body_x] = idNewSnake;
	map[tail_y][tail_x] = idNewSnake;

}