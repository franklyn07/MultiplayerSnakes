#ifndef NCURSES_H
#define NCURSES_H
	
#include <stdio.h>
#include <ncurses.h>
#include <pthread.h>
#include <unistd.h>
#include "../Deserialization/Deserialization.h"

//prints the map
void mapPrinter(int ** map, int MAPSIZE, int sockfd, int* points);

#endif