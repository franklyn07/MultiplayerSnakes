#include "./painter.h"

extern pthread_mutex_t client_lock;

//prints the map
void mapPrinter(int ** map, int MAPSIZE, int sockfd, int* points){

	//initialize window
	initscr();
	//dont echo key presses
	noecho();
	//dont display cursor
	curs_set(FALSE);
	//enable colour
	start_color();

	//create new window with border

	WINDOW *win = newwin(MAPSIZE+4,MAPSIZE+1,0,0);

	//init colour pairs
	init_pair(1,COLOR_RED,COLOR_RED);
	init_pair(2,COLOR_BLUE,COLOR_BLUE);
	init_pair(3,COLOR_GREEN,COLOR_GREEN);
	init_pair(4,COLOR_MAGENTA,COLOR_MAGENTA);
	init_pair(5,COLOR_CYAN,COLOR_CYAN);
	init_pair(6,COLOR_YELLOW,COLOR_YELLOW);

	//continously update screen
	while(1){
		wclear(win);

		wborder(win, '|', '|', '-', '-', '+', '+', '+', '+');
		wrefresh(win);	

		//lock for the map printing iteration
		pthread_mutex_lock(&client_lock);
		for (int i = 0; i < MAPSIZE; ++i){
			for (int j = 0; j < MAPSIZE; ++j){
				//get id of snake or if empty
				int currentVal = map[i][j];

				//if snake print colour if fruit print one colour only as well
				if(currentVal > 0){
					//we have 6 standard colours available apart from black and white
					switch(currentVal%6){
						case 0:
							wattron(win,COLOR_PAIR(1));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(1));
							break;
						case 1:
							wattron(win,COLOR_PAIR(2));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(2));
							break;
						case 2:
							wattron(win,COLOR_PAIR(5));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(5));
							break;
						case 3:
							wattron(win,COLOR_PAIR(3));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(3));
							break;
						case 4:
							wattron(win,COLOR_PAIR(4));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(4));
							break;
						case 5:
							wattron(win,COLOR_PAIR(6));
							mvwprintw(win,i,j,"%c",'o');
							wattroff(win,COLOR_PAIR(6));
							break;
					}
				}
			}
		}

		mvwhline(win, MAPSIZE,1,'-',MAPSIZE-1);
		mvwprintw(win,MAPSIZE+2,1,"Points: %d",*points);
		pthread_mutex_unlock(&client_lock);
		wrefresh(win);
		sleep(3);
	}

	endwin();

}