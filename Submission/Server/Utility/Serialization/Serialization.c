#include "./Serialization.h"

//serialize map
int serialize_map_size(char* buffer, int mapsize){

	buffer[0] = '/';
	buffer[1] = 'm';
	sprintf(buffer+2,"%d",mapsize);

	if(mapsize>9){
		buffer[4] = '\0';
	}else{
		buffer[3] = '\0';
	}
	
	//add 1 for null character
	return 4;
}

//serializes current map state
int serialize_current_map_state(char* buffer,int mapsize ,int** map){
	//go through map, every part of snake you find, list its id and coordinate
	int num_of_parts = 0;
	int counter = 2;

	buffer[0] = '/';
	buffer[1] = 'c';

	for(int i = 0; i< mapsize; i ++){
		for(int j = 0; j< mapsize; j++){
			//if not empty
			if(map[i][j] != 0){
				if(map[i][j]>9){
					sprintf(buffer+counter, "%d", map[i][j]);
				}else{
					sprintf(buffer+counter, "%d", 0);
					sprintf(buffer+counter+1, "%d", map[i][j]);
				}
				counter+=2;
				if(i>9){
					sprintf(buffer+counter, "%d", j);
				}else{
					sprintf(buffer+counter, "%d", 0);
					sprintf(buffer+counter+1, "%d", j);
				}
				counter+=2;
				if(j>9){
					sprintf(buffer+counter, "%d", i);
				}else{
					sprintf(buffer+counter, "%d", 0);
					sprintf(buffer+counter+1, "%d", i);
				}
				counter+=2;
				num_of_parts +=1;
			}
		}
	}

	sprintf(buffer+counter, "%c", '\0');
	counter+=1;
	return counter;
}

//serialize fruit position
int serialize_fruit(char * buffer, coordinate fruit_pos){
	int counter = 2;

	buffer[0] = '/';
	buffer[1] = 'f';

	if(fruit_pos.x>9){
		sprintf(buffer+counter, "%d", fruit_pos.x);
	}else{
		sprintf(buffer+counter, "%d", 0);
		sprintf(buffer+counter+1, "%d", fruit_pos.x);
	}
	counter+=2;
	if(fruit_pos.y>9){
		sprintf(buffer+counter, "%d", fruit_pos.x);
	}else{
		sprintf(buffer+counter, "%d", 0);
		sprintf(buffer+counter+1, "%d", fruit_pos.x);
	}
	counter+=2;

	sprintf(buffer+counter, "%c", '\0');
	return counter+1;
}

//serialize body of snake
int serialize_new_snake(char* buffer, coordinate head, coordinate body, coordinate tail, int idNewSnake){
	int counter = 2;

	buffer[0] = '/';
	buffer[1] = 'n';

	if(head.x > 9){
		sprintf(buffer+counter,"%d",head.x);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",head.x);		
	}
	counter+=2;

	if(head.y > 9){
		sprintf(buffer+counter,"%d",head.y);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",head.y);		
	}
	counter+=2;

	if(body.x > 9){
		sprintf(buffer+counter,"%d",body.x);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",body.x);		
	}
	counter+=2;

	if(body.y > 9){
		sprintf(buffer+counter,"%d",body.y);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",body.y);		
	}
	counter+=2;

	if(tail.x > 9){
		sprintf(buffer+counter,"%d",tail.x);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",tail.x);		
	}
	counter+=2;

	if(tail.y > 9){
		sprintf(buffer+counter,"%d",tail.y);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",tail.y);		
	}
	counter+=2;

	if(idNewSnake > 9){
		sprintf(buffer+counter,"%d",idNewSnake);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",idNewSnake);		
	}
	counter+=2;

	buffer[counter] = '\0';

	//add 1 for null character
	return counter+1;
}

int serialize_loser(char * buffer){
	buffer[0] = '/';
	buffer[1] = 'l';
	buffer[2] = '\0';

	return 3;
}

int serialize_points(char * buffer, int points){

	buffer[0] = '/';
	buffer[1] = 'p';

	if(points>9){
		sprintf(buffer+2,"%d",points);
	}else{
		sprintf(buffer+2,"%d",0);
		sprintf(buffer+3,"%d",points);
	}

	buffer[4] = '\0';

	return 5;
}

int serialize_head(char * buffer, coordinate newPosition, int snakeId){
	int counter = 2;

	buffer[0] = '/';
	buffer[1] = 'h';

	if(newPosition.x > 9){
		sprintf(buffer+counter,"%d",newPosition.x);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",newPosition.x);		
	}
	counter+=2;

	if(newPosition.y > 9){
		sprintf(buffer+counter,"%d",newPosition.y);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",newPosition.y);		
	}
	counter+=2;

	if(snakeId > 9){
		sprintf(buffer+counter,"%d",snakeId);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",snakeId);		
	}
	counter+=2;

	buffer[counter] = '\0';

	printf("Buffer Head : %s\n",buffer );

	//add 1 for null character
	return counter+1;
}

int serialize_tail(char * buffer, coordinate newPosition){
	int counter = 2;

	buffer[0] = '/';
	buffer[1] = 't';

	if(newPosition.x > 9){
		sprintf(buffer+counter,"%d",newPosition.x);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",newPosition.x);		
	}
	counter+=2;

	if(newPosition.y > 9){
		sprintf(buffer+counter,"%d",newPosition.y);
	}else{
		sprintf(buffer+counter,"%d",0);
		sprintf(buffer+counter+1,"%d",newPosition.y);		
	}
	counter+=2;

	buffer[counter] = '\0';

	printf("Buffer Tail : %s\n",buffer );

	//add 1 for null character
	return counter+1;
}