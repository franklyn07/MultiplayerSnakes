#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <stdio.h>
#include "../../Map/Map.h"

/*Special Characters
/m - Map size
/c - current state of map
/w - Winner
/l - Loser
/u - undecided (game still going)
/n - new snake
/h - list of head positions
/t - list of tail positions
/f = fruit position*/

//serialize map size
int serialize_map_size(char* buffer, int mapsize);

//serializes current map state
int serialize_current_map_state(char* buffer,int mapsize, int** map);

//serialize fruit position
int serialize_fruit(char * buffer, coordinate fruit_pos);

//serialize points
int serialize_points(char * buffer, int points);

//serializes new head position or tail postion
int serialize_head(char * buffer, coordinate newPosition, int snakeId);
int serialize_tail(char * buffer, coordinate newPosition);

//serializes loser message
int serialize_loser(char * buffer);

//serialize new snake
int serialize_new_snake(char* buffer, coordinate head, coordinate body, coordinate tail, int idNewSnake);

#endif