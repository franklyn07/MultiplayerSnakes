#include "./Threads_Server.h"
#include "../Server.h"

extern pthread_mutex_t lock;

//thread function that listens to connections
void *listenForClientsThread(void* arg){
	//create thread that updates users new position of snake
	pthread_t move, fruit;


	//get listen data and convert it
	listen_data_t * data = (listen_data_t*) arg;

	//assign values
	int sockfd = data->local_sockfd;
	socklen_t clilen = sizeof((data->cli_addr));

	//linked list of client socket ids
	struct ActiveConnection * connectionsHead = NULL;

	connectionsHead = malloc(sizeof(ActiveConnection));

	//check if malloc is succesful
	if (connectionsHead == NULL){
		error("Cannot allocate memory to connections linked list!\n");
		close(sockfd);
		pthread_exit(NULL);
	}

	//assign data to head of connections
	connectionsHead -> thread = -1;
	connectionsHead -> socket = -1;
	connectionsHead -> toDelete = 0;
	connectionsHead -> next = NULL;

	//linked list of socket ids to head coordinate
	struct SnakePosition * headsOfSnakes = NULL;

	headsOfSnakes = malloc(sizeof(SnakePosition));

	//check if malloc is succesful
	if (headsOfSnakes == NULL){
		close(sockfd);
		error("Cannot allocate memory to linked list!");
		pthread_exit(NULL);
	}

	//assign data to head of head positions
	headsOfSnakes -> socket = -1;
	headsOfSnakes -> x_coord = -1;
	headsOfSnakes -> y_coord = -1;
	headsOfSnakes -> next = NULL; 

	//linked list of socket ids to tail coordinate
	struct SnakePosition * tailsOfSnakes = NULL;

	tailsOfSnakes = malloc(sizeof(SnakePosition));

	//check if malloc is succesful
	if (tailsOfSnakes == NULL){
		error("Cannot allocate memory to linked list!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//assign data to head of head positions
	tailsOfSnakes -> socket = -1;
	tailsOfSnakes -> x_coord = -1;
	tailsOfSnakes -> y_coord = -1;
	tailsOfSnakes -> next = NULL;

	//linked list of socket ids to points
	struct Points * points_player = NULL;

	points_player = malloc(sizeof(Points));

	//check if malloc is succesful
	if (points_player == NULL){
		error("Cannot allocate memory to points list!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//assign data to head of head positions
	points_player -> socket = -1;
	points_player -> points = -1;
	points_player -> next = NULL;

	//linked list of socket ids to last direction
	struct Direction * direction_player = NULL;

	direction_player = malloc(sizeof(Direction));

	//check if malloc is succesful
	if (direction_player == NULL){
		error("Cannot allocate memory to directions list!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//assign data to head of head positions
	//note we use 'e' for erroneous input
	direction_player -> socket = -1;
	direction_player -> direction = 'e';
	direction_player -> next = NULL;

	//create struct which holds listener and list of connections
	//to be able to cleanup if anything happens
	struct clean_threads_data * cleanConnectionsData = malloc(sizeof(clean_threads_data));

	//check if malloc is succesful
	if (cleanConnectionsData == NULL){
		error("Cannot allocate memory cleaner!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//assign data to cleaners
	cleanConnectionsData -> socketOfListener = *(int*)arg;
	cleanConnectionsData -> connectionsHead = connectionsHead;
	cleanConnectionsData -> moveThread = move;
	cleanConnectionsData -> fruitThread = fruit;


	//will push thread on stack just in case early exit
	pthread_cleanup_push(listen_thread_cleanup,(void*)cleanConnectionsData);

	//will hold return from creating thread - if 0 created correctly
	int returnVal;

	//will hold address of new socket of client
	int newsockfd;

	//will hold thread temporarily
	pthread_t client;

	//catch signal
	signal(SIGINT,signalHandler);

	//create thread that updates users with fruit position
	//check if server full - if not create else stop client
	//create data for thread
	fruit_data_t * fruitData = malloc(sizeof(fruit_data_t));

	//check if malloc is succesful
	if (fruitData == NULL){
		error("Cannot allocate memory to fruit creator!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//allocating data
	fruitData -> connectionsHead = connectionsHead;
	fruitData -> map = data -> map;

	//create thread to update fruit position
	returnVal = pthread_create(&fruit, NULL, fruitThread, (void *) fruitData);

	//create thread to update move position
	//check if server full - if not create else stop client
	//create data for thread
	client_data_t * moveData = malloc(sizeof(client_data_t));

	//check if malloc is succesful
	if (moveData == NULL){
		error("Cannot allocate memory to move snakes thread!");
		close(sockfd);
		pthread_exit(NULL);
	}

	//allocating data
	moveData -> local_sockfd = newsockfd;
	moveData -> connectionsHead = connectionsHead;
	moveData -> headsOfSnakes = headsOfSnakes;
	moveData -> tailsOfSnakes = tailsOfSnakes;
	moveData -> direction_player = direction_player;
	moveData -> points_player = points_player;
	moveData -> map = data -> map;

	//create thread to handle the client
	returnVal = pthread_create(&move, NULL, moveThread, (void *) moveData);

	//creating the thread
	if(returnVal!=0){
		//delete new socket
		close(sockfd);
		error("Could not create thread to move snakes!");
		pthread_exit(NULL);
	}

	printf("Server config is done and ready to accept clients!\n");

	// Start listening for the clients
	listen(sockfd,10);

	//accept clients
	while(1){
		// Accept connection from a client
		newsockfd = accept(sockfd, (struct sockaddr *) data->cli_addr, &clilen);
		
		if (newsockfd < 0){
			error("Could not accept client!");
		}else{

			pthread_mutex_lock(&lock);
			int connections = getNumActiveConnections(connectionsHead);
			pthread_mutex_unlock(&lock);

			if(connections > MAPSIZE/2){
				//if server is full send null string to client
				int n;

				char buffer [256];

				bzero(buffer,256);

				buffer[0] = '\0';

				if ((n = write(newsockfd,&buffer,256)) < 0) {
					close(newsockfd);
					error("Not able to tell client that server is full\n");
				}

				error("Server is currently full!\n");
			}else{

				//check if server full - if not create else stop client
				//create data for thread
				client_data_t * clientData = malloc(sizeof(client_data_t));

				//check if malloc is succesful
				if (clientData == NULL){
					error("Cannot allocate memory cleaner!");
					pthread_exit(NULL);
				}

				clientData -> local_sockfd = newsockfd;
				clientData -> connectionsHead = connectionsHead;
				clientData -> headsOfSnakes = headsOfSnakes;
				clientData -> tailsOfSnakes = tailsOfSnakes;
				clientData -> direction_player = direction_player;
				clientData -> points_player = points_player;
				clientData -> map = data -> map;

				//create thread to handle the client
				returnVal = pthread_create(&client, NULL, handleClientThread, (void *) clientData);

				//creating the thread
				if(returnVal!=0){
					//delete new socket
					close(newsockfd);
					error("Could not create thread to handle client!");
				}
			}
		}

		/*//remove threads whose clients disconnected only one thread at a time is removed
		ActiveConnection * current = connectionsHead;
		pthread_mutex_lock(&lock);
		while(current!=NULL){
			if(current->toDelete == 1){
				printf("Before\n");
				fflush(stdout);
				print_list_sockets(connectionsHead);
				pthread_cancel(current->thread);
				printf("After\n");
				fflush(stdout);
				print_list_sockets(connectionsHead);
				break;
			}
			current = current -> next;
		}
		pthread_mutex_unlock(&lock);*/
	}

	pthread_cleanup_pop(1);

}

//thread that handles client upon connection
void *handleClientThread(void* arg){
	//pushing thread on stack so that it can be cleaned if exit form outside
	pthread_cleanup_push(client_thread_cleanup, arg);

	client_data_t * clientData = (client_data_t*)arg;

	int sockfd = clientData -> local_sockfd;

	//push thread data on shared linked list after user is sent map
	//using locks since shared
	pthread_mutex_lock(&lock);
	pushThread_Socket(clientData -> connectionsHead, pthread_self(),sockfd);
	int condition = initClient(sockfd,clientData -> connectionsHead ,clientData ->  headsOfSnakes, clientData -> tailsOfSnakes,clientData -> direction_player, clientData -> points_player , clientData -> map);
	//to enable client to recieve and perform init
	pthread_mutex_unlock(&lock);

	//if init was succesful start listening for commands else remove socket and clean thread
	if(condition != 0){
		//cancel self and this will call cleanup
		pthread_mutex_lock(&lock);
		setToDelete(clientData -> connectionsHead,sockfd);
		pthread_mutex_unlock(&lock);
	}else{
		//listen for commands
		printf("Init Concluded Normally!\n");

		int n;
		char buffer [256];
	
		while(1){
			bzero(buffer,256);

			sleep(1);

			if ((n = read(sockfd,&buffer,256)) < 0) {
				error("Not able to read input from client\n");
				pthread_mutex_lock(&lock);
				setToDelete(clientData -> connectionsHead,sockfd);
				pthread_mutex_unlock(&lock);
				//sleep to enable main thread to delete it
				sleep(100000);
			}

			printf("Recieved input : %c\n",buffer[2]);

			//if user disconnects
			if(buffer[2] == 'u'){
				pthread_mutex_lock(&lock);
				//remove sockets
				removeFromLists(sockfd,clientData -> connectionsHead ,clientData -> points_player,clientData -> direction_player, clientData ->  headsOfSnakes, clientData -> tailsOfSnakes);
				
				//remove snake from local map
				removeSnake(clientData->map, sockfd, buffer);
				pthread_mutex_unlock(&lock);

				//empty buffer from rubbish data filled by removeSnake
				bzero(buffer,256);

				//close socket
				close(sockfd);
				
				sleep(100000);
			}

			//if not unrecognisable character update in directionlist
			if(buffer[2]!=(char)'\0'){
				pthread_mutex_lock(&lock);
				//get last direction
				char last_direction = getLastDirection(clientData -> direction_player, sockfd);

				switch(last_direction){
					case 'w':
						if(buffer[2] != (char)'s'){
							updateDirection(clientData -> direction_player, sockfd, buffer[2]);
						}
						break;
					case 'a':
						if(buffer[2] != (char)'d'){
							updateDirection(clientData -> direction_player, sockfd, buffer[2]);
						}
						break;
					case 's':
						if(buffer[2] != (char)'w'){
							updateDirection(clientData -> direction_player, sockfd, buffer[2]);
						}
						break;
					case 'd':
						if(buffer[2] != (char)'a'){
							updateDirection(clientData -> direction_player, sockfd, buffer[2]);
						}
						break;
				}
				pthread_mutex_unlock(&lock);				
			}
		}
	}

	pthread_exit(NULL);

	//popping from stack if early kill
	pthread_cleanup_pop(1);
}

//initialises client and its attributes are added to state lists
int initClient (int sock,struct ActiveConnection* connectionsHead, SnakePosition* headsOfSnakes, SnakePosition* tailsOfSnakes,Direction * direction_player, Points * points_player, int ** map){
	int n;
	char buffer[256];

	//generate coordinate for head
	struct coordinate head_coord = generateHead(map);

	//check it is not invalid
	if(head_coord.x == -1){
		error("Server couldn't find space for new snake!\n");
		return -1;
	}

	//puts snake on map and returns its tail lagging by 1
	//further desc in Map.h
	struct coordinate tail_coord = addSnake(sock, head_coord, map);

	printf("AFTER INIT\n");
	printMap(map);

	//building new structs to hold head and tail of position
	struct SnakePosition* newHead = malloc(sizeof(SnakePosition));

	//check if malloc is succesful
	if (newHead == NULL){
		error("Cannot allocate memory to new head coordinate!\n");
		return -1;
	}

	//assign data to head of head positions
	newHead -> socket = sock;
	newHead -> x_coord = head_coord.x;
	newHead -> y_coord = head_coord.y;
	newHead -> next = NULL;

	//building new structs to hold head and tail of position
	struct SnakePosition* newTail = malloc(sizeof(SnakePosition));

	//check if malloc is succesful
	if (newTail == NULL){
		error("Cannot allocate memory to new tail coordinate!");
		return -1;
	}

	//assign data to head of tail positions
	newTail -> socket = sock;
	newTail -> x_coord = tail_coord.x;
	newTail -> y_coord = tail_coord.y;
	newTail -> next = NULL;

	//building new structs to hold points of player
	struct Points* newPlayerPoints = malloc(sizeof(Points));

	//check if malloc is succesful
	if (newPlayerPoints == NULL){
		error("Cannot allocate memory to new player_points coordinate!");
		return -1;
	}

	//assign data to head of head positions
	newPlayerPoints -> socket = sock;
	newPlayerPoints -> points = 0;
	newPlayerPoints -> next = NULL;

	//building new structs to hold direction of player
	struct Direction* newPlayerDirection = malloc(sizeof(Direction));

	//check if malloc is succesful
	if (newPlayerDirection == NULL){
		error("Cannot allocate memory to new player_direction coordinate!");
		return -1;
	}

	//assign data to head of head positions
	newPlayerDirection -> socket = sock;
	newPlayerDirection -> direction = getDirection(head_coord.x, head_coord.y, tail_coord.x, tail_coord.y);
	newPlayerDirection -> next = NULL;

	//update list of heads and list of tails
	addPosition(headsOfSnakes,newHead);
	addPosition(tailsOfSnakes,newTail);
	addPlayerPoints(points_player,newPlayerPoints);
	addPlayerDirection(direction_player,newPlayerDirection);

	//update all connections
	//if socket is new player send him size of map and snake, else send him new snake only
	ActiveConnection * current = connectionsHead;

	while(current != NULL){
		int currentSocket = current->socket;
		printf("I am socket: %d\n",sock);
		printf("I am Visiting socket: %d\n", currentSocket);
		fflush(stdout);
		if(currentSocket > 0 && currentSocket == sock){
			printf("I am Sending to socket: %d",currentSocket);
			
			bzero(buffer,256);

			//serialize map size
			int size = serialize_map_size(buffer,MAPSIZE);

			printf("Sent Mapsize: %s\n",buffer);

			if ((n = write(currentSocket,&buffer,size)) < 0) {
				error("ERROR writing to socket\n");
				return -1;
			}

			//to enable user to read map
			sleep(1);

			//serialize current map state
			bzero(buffer,256);
			size = serialize_current_map_state(buffer,MAPSIZE,map);

			printf("Sent Current Map: %s\n",buffer);


			if ((n = write(currentSocket,&buffer,size)) < 0) {
				error("ERROR writing to socket\n");

				return -1;
			}

		}else if (currentSocket > 0){
			printf("Sending to socket: %d\n",currentSocket);
			bzero(buffer,256);

			//serialize new snake
			struct coordinate body = getBody(head_coord,tail_coord);
			struct coordinate tail = getTail(head_coord,body);

			if(body.x == -1 || tail.x == -1){
				error("Error Occured in finding snake body|tail\n");
				
				return -1;
			}

			int size = serialize_new_snake(buffer,head_coord,body,tail,sock);


			printf("Buffer Sent new snake: %s\n",buffer);

			if ((n = write(currentSocket,&buffer,size)) < 0) {
				error("ERROR writing to socket\n");

				return -1;
			}

		}
		current = current -> next;
	}

	return 0;
}

void* fruitThread(void * arg){
	fruit_data_t * fruitData = (fruit_data_t *)arg;

	int ** map = fruitData -> map;

	//holds number of active players
	int size;

	//actively check if any connection was added - once connected start updating
	while(1){
		//get size of local connections
		pthread_mutex_lock(&lock);
		size = getNumActiveConnections(fruitData->connectionsHead);
		pthread_mutex_unlock(&lock);

		if(size>0){
			break;
		}else{
			sleep(0.5);
		}
	}

	int n;

	char buffer [256];

	int size_serial;

	//keep sending fruit as long as there are a number of players
	LOOP: while(1){
		//this will sleep between 30 to 60 seconds - randomly
		sleep(generateRandomInt(30)+30);

		pthread_mutex_lock(&lock);
		struct coordinate fruit_pos = generateFruit(fruitData->map);

		//if -1 not an available space was found in that iteration
		if(fruit_pos.x == -1 && fruit_pos.y == -1){
			pthread_mutex_unlock(&lock);
			goto LOOP;
		}else{
			//our fruit value will be 126 = '~'
			//update map locally
			map[fruit_pos.y][fruit_pos.x] = 126;

			//send update to all users
			//if server is full send null string to client
			bzero(buffer,256);

			size_serial = serialize_fruit(buffer, fruit_pos);

			printf("New Fruit: %s\n",buffer);

			//update all connections
			//if socket is new player send him size of map and snake, else send him new snake only
			ActiveConnection * current = fruitData -> connectionsHead;

			while(current != NULL){

				if(current -> socket > 0){
					if ((n = write(current->socket,&buffer,size_serial)) < 0) {
						error("Not able to write to client\n");
						//remove socket from lists by stopping thread of that client and cleaning it up 
						current -> toDelete = 1;
						goto LOOP;
					}
				}

				current = current->next;
			}
	
		}
		pthread_mutex_unlock(&lock);	
	}
	pthread_exit(NULL);
}

//thread that handles the game logic with regards to points and movement
void *moveThread(void * arg){
	client_data_t * moveData = (client_data_t *)arg;

	int ** map = moveData -> map;

	//holds number of active players
	int size;

	//actively check if any connection was added - once connected start updating
	while(1){
		//get size of local connections
		pthread_mutex_lock(&lock);
		size = getNumActiveConnections(moveData->connectionsHead);
		pthread_mutex_unlock(&lock);

		if(size>0){
			break;
		}else{
			sleep(1);
		}
	}

	int n;

	char buffer [256];

	//keep sending moves as long as there are a number of players
	while(1){

		//sleep before sending update
		sleep(1.5);

		pthread_mutex_lock(&lock);

		//iterate through all connections
		ActiveConnection * current = moveData -> connectionsHead;

		while(current != NULL){
			int current_socket = current -> socket;
			if(current_socket > 0){
				//get last direction of player
				char last_direction = getLastDirection(moveData -> direction_player,current_socket);
				
				///get last position of sneak head of player
				struct coordinate current_head_pos = getCurrentPosition(moveData -> headsOfSnakes, current_socket);

				int x_coord = current_head_pos.x;
				int y_coord = current_head_pos.y;
				struct coordinate new_head_pos;

				//update his head depending on the direction
				switch(last_direction){
					case 'w':
						if(x_coord != -1 && y_coord != -1){
							//will hold updated coord of head
							new_head_pos.x = x_coord;
							new_head_pos.y = y_coord-1;
						}

						break;
					case 'a':
						if(x_coord != -1 && y_coord != -1){
							//will hold updated coord of head
							new_head_pos.x = x_coord-1;
							new_head_pos.y = y_coord;
						}
						break;
					case 's':
						if(x_coord != -1 && y_coord != -1){
							//will hold updated coord of head
							new_head_pos.x = x_coord;
							new_head_pos.y = y_coord+1;
						}
						break;
					case 'd':
						if(x_coord != -1 && y_coord != -1){
							//will hold updated coord of head
							new_head_pos.x = x_coord+1;
							new_head_pos.y = y_coord;
						}

						break;
					case '\0':
						//miss update and do next iteration
						goto LOOP;
				}


				printf("new_head_pos.x,new_head_pos.y: %d,%d\n",new_head_pos.x,new_head_pos.y );
				sleep(1);

				//check new head position if out of bounds
				if(new_head_pos.x < 0 || new_head_pos.x >= MAPSIZE ||
					new_head_pos.y < 0 || new_head_pos.y >= MAPSIZE){

					bzero(buffer,256);

					//put parts of snake to remove in buffer, whilt removing them locally
					size = removeSnake(moveData -> map, current_socket, buffer);

					printf("Buffer %s\n",buffer );

					//iterate through all connections
					ActiveConnection * currentConnection = moveData -> connectionsHead;

					while(currentConnection != NULL){
						int currentSock = currentConnection -> socket;
						if(currentSock > 0){
							if ((n = write(currentSock,&buffer,size)) < 0) {
								error("Not able to tell client position of head of snake\n");
								//remove socket from lists
								//delete all socket attributes from list, and close its socket and thread
								currentConnection -> toDelete =1;
							}
						}
						currentConnection = currentConnection -> next;
					}
					
					sleep(1);

					//send him he is a loser
					bzero(buffer,256);

					int size = serialize_loser(buffer);

					if ((n = write(current_socket,&buffer,size)) < 0) {
						error("Not able to tell client that he is a loser\n");
						//remove socket from lists
						//delete all socket attributes from list, and close its socket and thread
						current -> toDelete =1;
					}					

					//delete all socket attributes from list, and close its socket and thread
					//current->toDelete = 1;
					removeFromLists(current_socket,moveData -> connectionsHead ,moveData -> points_player,moveData -> direction_player, moveData ->  headsOfSnakes, moveData -> tailsOfSnakes);

					//close socket
					close(current_socket);

					//check next node
					goto LOOP;
				}

				//check if head is onto another player
				if(map[new_head_pos.y][new_head_pos.x] > 0 && map[new_head_pos.y][new_head_pos.x] != 126){

					bzero(buffer,256);

					//put parts of snake to remove in buffer
					size = removeSnake(moveData -> map, current_socket, buffer);

					printf("Buffer %s\n",buffer );

					//iterate through all connections
					ActiveConnection * currentConnection = moveData -> connectionsHead;

					while(currentConnection != NULL){
						int currentSock = currentConnection -> socket;
						if(currentSock > 0){
							if ((n = write(currentSock,&buffer,size)) < 0) {
								error("Not able to tell client position of head of snake\n");
								//remove socket from lists
								//delete all socket attributes from list, and close its socket and thread
								currentConnection -> toDelete =1;
							}
						}
						currentConnection = currentConnection -> next;
					}

					sleep(1);
				
					//send him he is a loser
					bzero(buffer,256);

					int size = serialize_loser(buffer);

					if ((n = write(current_socket,&buffer,size)) < 0) {
						error("Not able to tell client that he is a loser\n");
						//TODO::remove socket from lists
						//delete all socket attributes from list, and close its socket and thread
						current -> toDelete =1;
					}

					//delete all socket attributes from list, and close its socket and thread
					//current->toDelete = 1;
					removeFromLists(current_socket,moveData -> connectionsHead ,moveData -> points_player,moveData -> direction_player, moveData ->  headsOfSnakes, moveData -> tailsOfSnakes);
					
					//close socket
					close(current_socket);

					//check next node
					goto LOOP;
				}

				//check if head is onto a fruit
				if(map[new_head_pos.y][new_head_pos.x] == 126){
					int returnVal;

					//update head position in list
					if((returnVal = updatePosition(moveData -> headsOfSnakes, current_socket, new_head_pos))<0){
						error("Failed to update position");
						goto LOOP;
					}

					//update head position on map
					map[new_head_pos.y][new_head_pos.x] = current_socket;

					//get current points
					int currentPoints = getCurrentPoints(moveData -> points_player, current_socket);

					if(currentPoints != -1){
						currentPoints+=1;
						//update points in list
						if((returnVal = updatePoints(moveData -> points_player, current_socket,currentPoints))<0){
							error("Failed to update points");
							goto LOOP;
						}
					}else{
						error("Failed to find player in points list!");
						goto LOOP;
					}

					//check if winner
					if(currentPoints == 15){
						//update him he is winner and all the others they are losers

						//iterate through all connections
						ActiveConnection * currentConnection = moveData -> connectionsHead;

						while(currentConnection != NULL){
							int currentSock = currentConnection -> socket;
							if(currentSock > 0){
								sleep(1);
								bzero(buffer,256);
								if(currentSock == current_socket){
									
									buffer[0] = '/';
									buffer[1] = 'w';
									buffer[2] = '\0';

									if ((n = write(currentSock,&buffer,3)) < 0) {
										error("Not able to tell client he is a winner\n");
										//remove socket from lists
										//delete all socket attributes from list, and close its socket and thread
										currentConnection -> toDelete =1;
									}
								}else{

									buffer[0] = '/';
									buffer[1] = 'l';
									buffer[2] = '\0';

									if ((n = write(currentSock,&buffer,3)) < 0) {
										error("Not able to tell client he is a loser\n");
										//remove socket from lists
										//delete all socket attributes from list, and close its socket and thread
										currentConnection -> toDelete =1;
									}
								}
							}
							currentConnection = currentConnection -> next;
						}

						//stop iterating coz game finished
						break;
					}

					//update all clients with head position
					//tail is not updated since we add one piece to the front of snake
					//and  tail remains where it is
					bzero(buffer,256);

					int size = serialize_head(buffer,new_head_pos,current_socket);

					//iterate through all connections
					ActiveConnection * currentConnection = moveData -> connectionsHead;

					while(currentConnection != NULL){
						int currentSock = currentConnection -> socket;
						if(currentSock > 0){
							if ((n = write(currentSock,&buffer,size)) < 0) {
								error("Not able to tell client position of head of snake\n");
								//remove socket from lists
								//delete all socket attributes from list, and close its socket and thread
								currentConnection -> toDelete =1;
							}
						}
						currentConnection = currentConnection -> next;
					}

					//sleep befoer sending points
					sleep(1);

					//update client with new amount of points
					bzero(buffer,256);

					buffer[0] = '/';
					buffer[1] = 'p';
					buffer[2] = '\0';

					if ((n = write(current_socket,&buffer,3)) < 0) {
						error("Not able to tell client his current points\n");
						//TODO::remove socket from lists
						//delete all socket attributes from list, and close its socket and thread
						currentConnection -> toDelete = 1;
					}
					goto LOOP;
				}

				//if empty
				if(map[new_head_pos.y][new_head_pos.x] == 0){

					int returnVal;

					//update head position in list
					if((returnVal = updatePosition(moveData -> headsOfSnakes, current_socket, new_head_pos))<0){
						error("Failed to update position\n");
						goto LOOP;
					}

					//update head position on map
					map[new_head_pos.y][new_head_pos.x] = current_socket;

					//get old tail position
					struct coordinate old_tail = getCurrentPosition(moveData -> tailsOfSnakes, current_socket);

					//pos for new tail
					struct coordinate new_tail;

					print_list_positions(moveData -> headsOfSnakes);

					if(old_tail.x != -1 && old_tail.y != -1){
						//get new tail position
						new_tail = getNewTail(map, old_tail, current_socket);
					}else{
						error("Failed to find old tail in list position \n");
						goto LOOP;
					}

					//update new tail in list
					if((returnVal = updatePosition(moveData -> tailsOfSnakes, current_socket, new_tail))<0 ){
						error("Failed to update tail list with new tail\n");
						goto LOOP;
					}

					//update tail position in list by deleting last body part
					map[new_tail.y][new_tail.x] = 0;

					printMap(map);

					//update clients with new tail
					bzero(buffer,256);

					int size = serialize_tail(buffer,new_tail);

					//iterate through all connections
					ActiveConnection * currentConnection = moveData -> connectionsHead;

					while(currentConnection != NULL){
						int currentSock = currentConnection -> socket;
						if(currentSock > 0){
							if ((n = write(currentSock,&buffer,size)) < 0) {
								error("Not able to tell client position of tail of snake\n");
								//TODO::remove socket from lists
								//delete all socket attributes from list, and close its socket and thread
								currentConnection->toDelete =1;
							}
						}
						currentConnection = currentConnection -> next;
					}

					//sleep before sending head
					sleep(1);

					//update client with head
					//tail is not updated since we add one piece to the front of snake
					//and  tail remains where it is
					bzero(buffer,256);

					size = serialize_head(buffer,new_head_pos,current_socket);
					
					//iterate through all connections by updating pointer
					currentConnection = moveData -> connectionsHead;

					while(currentConnection != NULL){
						int currentSock = currentConnection -> socket;
						if(currentSock > 0){
							if ((n = write(currentSock,&buffer,size)) < 0) {
								error("Not able to tell client position of tail of snake\n");
								//TODO::remove socket from lists
								//delete all socket attributes from list, and close its socket and thread
								currentConnection -> toDelete = 1;
							}
						}
						currentConnection = currentConnection -> next;
					}
					goto LOOP;
				}
			}

			LOOP: current = current->next;
		}

		pthread_mutex_unlock(&lock);
	}

	pthread_exit(NULL);
}


//client handling thread cleaner
void listen_thread_cleanup(void * arg){
	//extracting data
	struct clean_threads_data* data = (struct clean_threads_data *) arg;

	//close all threads
	ActiveConnection * current = data->connectionsHead;

	//to skip empty head
	current = current->next;

	//used for transimission
	char buffer[256];
	int n;
	while (current != NULL) {
		//cancel thread which will invoke socket closing
		if(current->socket > 0){
			sleep(1);
			bzero(buffer,256);

			//flag connected that server is down
			buffer[0] = '/';
			buffer[1] = 'e';
			buffer[2] = '\0';

			if ((n = write(current->socket,&buffer,3)) < 0) {
				error("Not able to tell client that server is down\n");
			}

		}
		pthread_cancel(current->thread);
		current = current->next;
	}

	//cancel all threads
	pthread_cancel(data->moveThread);
	pthread_cancel(data->fruitThread);

	//close listener socket
	close(data->socketOfListener);
	printf("Cleaning Up Socket: %d\n", data->socketOfListener);
	fflush(stdout);
}

//client handling thread cleaner
void client_thread_cleanup(void * arg){
	client_data_t * clientData = (client_data_t*)arg;
	
	int sockfd = clientData  -> local_sockfd;

	//remove socket from all lists
	removeFromLists(sockfd,clientData -> connectionsHead ,clientData -> points_player,clientData -> direction_player, clientData ->  headsOfSnakes, clientData -> tailsOfSnakes);

	//close socket
	close(sockfd);
	printf("Cleaning Up Socket: %d\n", sockfd);
	fflush(stdout);
}

//remove socket from all lists
void removeFromLists(int sockfd,ActiveConnection * headConnection, Points* headPoints, Direction* headDirection, SnakePosition* headHeads, SnakePosition* headTails){
	removeSocket(&headConnection,sockfd);
	removePoints(&headPoints,sockfd);
	removePosition(&headHeads,sockfd);
	removePosition(&headTails,sockfd);
	removePlayer(&headDirection,sockfd);
}