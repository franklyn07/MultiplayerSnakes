#ifndef THREADS_SERVER_H
#define THREADS_SERVER_H

#include "../LinkedList/ActiveConnectionsList.h"
#include "../LinkedList/PositionSocketList.h"
#include "../LinkedList/PointsList.h"
#include "../LinkedList/DirectionsList.h"
#include "../Utility/Serialization/Serialization.h"

//struct to hold the listen thread data
typedef struct listen_data{
	int local_sockfd;
	struct sockaddr_in* cli_addr;
	int ** map;
}listen_data_t;

//struct to hold the client thread data
typedef struct client_data{
	int local_sockfd;
	struct ActiveConnection* connectionsHead;
	struct SnakePosition * headsOfSnakes;
	struct SnakePosition * tailsOfSnakes;
	struct Direction * direction_player;
	struct Points * points_player;
	int ** map;
}client_data_t;

//stuct that holds info for fruit ceation data
typedef struct fruit_data{
	int ** map;
	struct ActiveConnection* connectionsHead;
}fruit_data_t;

//struct to hold the data for cleaning up the threads
typedef struct clean_threads_data{
	int socketOfListener;
	struct ActiveConnection * connectionsHead;
	pthread_t fruitThread;
	pthread_t moveThread;
}clean_threads_data;

//thread function that listens to connections
void *listenForClientsThread(void* arg);

//initialises client
// return 0 on success and -1 on failure
int initClient (int sock,ActiveConnection* connectionsHead, SnakePosition* headsOfSnakes, SnakePosition* tailsOfSnakes,Direction * direction_player, Points * points_player, int ** map);

//thread that handles client
void *handleClientThread(void* arg);

//thread that creates fruit
void *fruitThread(void * arg);

//thread that moves the snakes, updates points and checks for winner
void *moveThread(void * arg);

//method that removes socket from all lists
void removeFromLists(int sockfd,ActiveConnection * headConnection, Points* headPoints, Direction* headDirection, SnakePosition* headHeads, SnakePosition* headTails);

//thread cleaners
void client_thread_cleanup(void * arg);
void listen_thread_cleanup(void * arg);

#endif