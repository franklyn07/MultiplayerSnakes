#ifndef ACTIVE_LIST_H
#define ACTIVE_LIST_H

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

//ActiveConnections that will hold generic data in the linked list
typedef struct ActiveConnection{
	pthread_t thread;
	int socket;
	//if 0 dont delete - if 1 delete
	int toDelete;
	struct ActiveConnection * next;
}ActiveConnection;

//will iterate over the list and print its values
//debugging purposes
void print_list_sockets(struct ActiveConnection * head);

//will add item with 1 value to the end of the list
void pushThread(struct ActiveConnection * head, pthread_t thread);

//will add item with 2 value to the end of the list
void pushThread_Socket(struct ActiveConnection * head, pthread_t thread, int socket);

//remove a specific item from the list using first value aka thread
//return 0 on succes and -1 if not successful
int removeSocket(struct ActiveConnection ** head, int socket);

//update socket, after finding first thread
//return 0 on succes and -1 if not successful
int updateSocket(struct ActiveConnection* head, pthread_t thread, int socket);

//we search for thread and we get the socket in the same ActiveConnection
int getSocket(struct ActiveConnection* head, pthread_t thread);

//we search for a socket and get the thread in the same active connection
pthread_t getThread(struct ActiveConnection* head, int socket);

//adds node to current linked list
void addConnection(struct ActiveConnection* head, ActiveConnection* connection);

//sets toDelete value to 1
void setToDelete(struct ActiveConnection* head, int socket);

//gets number of active connections
int getNumActiveConnections(struct ActiveConnection* head);

#endif