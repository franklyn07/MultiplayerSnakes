#ifndef DIRECTIONS_LIST_H
#define DIRECTIONS_LIST_H

#include <stdlib.h>
#include <stdio.h>

//will hold the socket, and last direction inputted by player
typedef struct Direction{
	int socket;
	char direction;
	struct Direction * next;
}Direction;

//will iterate over the list and print its values
//debugging purposes
void print_list_directions(Direction * head);

//remove a specific player from list using socket 
//0 on success and -1 if not successful
int removePlayer(Direction ** head, int socket);

//update Direction, after finding the socket
//return 0 on succes and -1 if not successful
int updateDirection(Direction* head, int socket, char newDirection);

//adds node to current linked list
void addPlayerDirection(Direction* head, Direction* newPlayer);

//returns last direction of player
char getLastDirection(Direction* head, int socket);

#endif