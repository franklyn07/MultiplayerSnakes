#include "PositionSocketList.h"

//print a list of values
void print_list_positions(SnakePosition * head) {
    SnakePosition * current = head;

    while (current != NULL) {
        printf("Socket: %d\t Position: %d,%d\n",current->socket,current->x_coord, current->y_coord);
        fflush(stdout);
        current = current->next;
    }
}

//adds node to current linked list
void addPosition(SnakePosition* head, SnakePosition* position){
    SnakePosition* current = head;


    //iterate through linked list to go to final node
    while (current->next != NULL) {
        current = current->next;
    }

    //add new node to end of list
    current->next = position;
}

//remove a specific position from list using socket 
//0 on success and -1 if not successful
int removePosition(SnakePosition ** head, int socket){
     for(SnakePosition ** current = head; *current != NULL; current = &(*current)->next){
        if((*current)->socket == socket){
            SnakePosition * next = (*current)->next;
            free (*current);
            *current = next;
            return 0;
        }
    }
    return -1;
}

//update position, after finding the socket
//return 0 on succes and -1 if not successful
int updatePosition(SnakePosition* head, int socket, coordinate coord){
    SnakePosition * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            current-> x_coord = coord.x;
            current-> y_coord = coord.y;
            return 0;
        }
        current = current->next;
    }

    return -1;
}

//returns current position
coordinate getCurrentPosition(SnakePosition* head, int socket){
    SnakePosition * current = head;

    //iterate through linked list
    while (current != NULL) {
        //if we find the value update the second value
        if(current->socket == socket){
            struct coordinate coord = {current->x_coord,current->y_coord};
            return coord;
        }
        current = current->next;
    }

    struct coordinate coord = {-1,-1};

    return coord;
}